from fastapi import FastAPI, Request
from fastapi.exceptions import RequestValidationError
from starlette.responses import JSONResponse

from oauthlaw_app.api.oauth_api import oauth_route
from oauthlaw_app.database import models
from oauthlaw_app.database.session import engine
from oauthlaw_app.exceptions import CustomException

models.Base.metadata.create_all(bind=engine)

app = FastAPI()
app.include_router(oauth_route, prefix='/oauth')


@app.exception_handler(RequestValidationError)
async def request_validation_exception_handler(request: Request, exception: RequestValidationError):
    error_message = []
    for error in exception.errors():
        error_location = ''
        for error_loc in error["loc"]:
            error_location += f'{error_loc} - '
        error_location = error_location[:-3]

        error_message.append(f'{error["msg"]}: {error_location}')

    return JSONResponse(
        status_code=422,
        content={
            'error': 'Unprocressable Entity',
            'error_detail': error_message
        }
    )


@app.exception_handler(CustomException)
async def custom_exception_handler(request: Request, exception: CustomException):
    return JSONResponse(
        status_code=exception.status_code,
        content=exception.content
    )
