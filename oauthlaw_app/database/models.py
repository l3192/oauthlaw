
from sqlalchemy import Column, Integer, ForeignKey, DateTime, String
from oauthlaw_app.database.session import Base

schema = {'schema': 'oauthlaw'}


class User(Base):
    __tablename__ = 'user'
    __table_args__ = schema

    id = Column(Integer, primary_key=True)
    username = Column(String)
    password = Column(String)
    full_name = Column(String)
    npm = Column(String)


class Client(Base):
    __tablename__ = 'client'
    __table_args__ = schema

    id = Column(Integer, primary_key=True)
    client_id = Column(String)
    client_secret = Column(String)


class Token(Base):
    __tablename__ = 'token'
    __table_args__ = schema

    id = Column(Integer, primary_key=True)
    access_token = Column(String)
    refresh_token = Column(String)
    user_id = Column(String, ForeignKey('oauthlaw.user.id'))
    client_id = Column(String, ForeignKey('oauthlaw.client.id'))
    expired_at = Column(DateTime)
