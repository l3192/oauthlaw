from datetime import datetime, timedelta
from typing import Optional, List

from sqlalchemy.orm import Session

from oauthlaw_app.database import models, schemas


def get_client_by_id(session: Session, id: int) -> Optional[models.Client]:
    return session.query(models.Client).filter(models.Client.id == id).first()


def get_client_by_client_id(session: Session, client_id: str) -> Optional[models.Client]:
    return session.query(models.Client).filter(models.Client.client_id == client_id).first()


def get_user_by_username(session: Session, username: str) -> Optional[models.User]:
    return session.query(models.User).filter(models.User.username == username).first()


def get_access_token(session: Session) -> List[str]:
    result = session.query(models.Token.access_token).all()
    return [res.access_token for res in result]


def get_refresh_token(session: Session) -> List[str]:
    result = session.query(models.Token.refresh_token).all()
    return [res.refresh_token for res in result]


def insert_authentication_token(session: Session, token: schemas.TokenBase) -> models.Token:
    token = models.Token(**token.dict(), expired_at=datetime.now() + timedelta(seconds=300))
    session.add(token)
    session.commit()
    session.refresh(token)
    return token


def get_token_by_access_token(session: Session, access_token: str) -> Optional[models.Token]:
    return session.query(models.Token).filter(models.Token.access_token == access_token).first()


def get_user_by_access_token(session: Session, access_token: str) -> Optional[schemas.User]:
    result = session.query(models.User)\
        .join(models.Token, models.Token.user_id == models.User.id)\
        .filter(models.Token.access_token == access_token)\
        .first()

    if result:
        return schemas.User(id=result.id, username=result.username, password=result.password,
                            full_name=result.full_name, npm=result.npm)


def delete_expired_token(session: Session) -> None:
    session.query(models.Token).filter(models.Token.expired_at + timedelta(minutes=60) < datetime.now()).delete()
    session.commit()
