from datetime import datetime

from pydantic import BaseModel


class User(BaseModel):
    id: int
    username: str
    password: str
    full_name: str
    npm: str

    class Config:
        orm_mode = True


class Client(BaseModel):
    id: int
    client_id: str
    client_secret: str

    class Config:
        orm_mode = True


class TokenBase(BaseModel):
    access_token: str
    refresh_token: str
    user_id: int
    client_id: int


class Token(TokenBase):
    id: int
    expired_at: datetime

    class Config:
        orm_mode = True
