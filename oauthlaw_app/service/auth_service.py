from datetime import datetime

from sqlalchemy.orm import Session

from oauthlaw_app.database.accessor import get_user_by_access_token, get_token_by_access_token, delete_expired_token
from oauthlaw_app.exceptions import CustomException


def login(session: Session, authorization_token: str):
    delete_expired_token(session=session)

    auth_splitted = authorization_token.split(' ')
    if len(auth_splitted) != 2 or auth_splitted[0] != 'Bearer':
        raise CustomException(
            status_code=403,
            content={'error': 'Invalid Request', 'error_description': 'Authorization must be Bearer token type!'}
        )

    token = get_token_by_access_token(session=session, access_token=auth_splitted[1])
    if not token:
        raise CustomException(
            status_code=401,
            content={'error': 'Unauthorized', 'error_description': 'Invalid credentials'}
        )
    if token.expired_at < datetime.now():
        raise CustomException(
            status_code=401,
            content={'error': 'Unauthorized', 'error_description': 'Token expired'}
        )

    user = get_user_by_access_token(session=session, access_token=auth_splitted[1])
    if not user:
        raise CustomException(
            status_code=401,
            content={'error': 'Unauthorized', 'error_description': 'User not found'}
        )
    return user, token
