import random
import string
from typing import List

from sqlalchemy.orm import Session

from oauthlaw_app.database import models
from oauthlaw_app.database.accessor import get_access_token, get_refresh_token, insert_authentication_token, \
    delete_expired_token
from oauthlaw_app.database.schemas import TokenBase


def generate_new_access_token_and_refresh_token(session: Session, user_id: int, client_id: int) -> models.Token:
    delete_expired_token(session=session)

    all_access_token = get_access_token(session=session)
    all_refresh_token = get_refresh_token(session=session)
    all_token = all_access_token + all_refresh_token

    access_token = generate_token(all_token=all_token)
    all_token.append(access_token)
    refresh_token = generate_token(all_token=all_token)

    insert_token_spec = TokenBase(access_token=access_token, refresh_token=refresh_token, user_id=user_id, client_id=client_id)
    return insert_authentication_token(session=session, token=insert_token_spec)


def generate_token(all_token: List[str]) -> str:
    token = ''.join(random.choices(string.ascii_letters, k=40))
    if token in all_token:
        token = generate_token(all_token=all_token)
    return token
