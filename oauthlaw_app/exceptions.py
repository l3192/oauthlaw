from typing import Dict


class CustomException(Exception):
    def __init__(self, status_code: int, content: Dict):
        self.status_code = status_code
        self.content = content
