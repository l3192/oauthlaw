from datetime import datetime
from typing import Optional

from fastapi import APIRouter, Depends, Form, Request, Header
from pydantic import BaseModel
from sqlalchemy.orm import Session

from oauthlaw_app.database.accessor import get_client_by_client_id, get_user_by_username, get_client_by_id
from oauthlaw_app.database.session import get_session
from oauthlaw_app.exceptions import CustomException
from oauthlaw_app.service.token_service import generate_new_access_token_and_refresh_token
from oauthlaw_app.service.auth_service import login

oauth_route = APIRouter()


class ErrorResponse(BaseModel):
    error: str
    error_description: str


class TokenRequest(BaseModel):
    username: str
    password: str
    grant_type: str
    client_id: str
    client_secret: str

    @classmethod
    def as_form(cls,
                username: str = Form(...),
                password: str = Form(...),
                grant_type: str = Form(...),
                client_id: str = Form(...),
                client_secret: str = Form(...)) -> 'TokenRequest':
        return cls(username=username, password=password, grant_type=grant_type, client_id=client_id,
                   client_secret=client_secret)


class TokenResponse(BaseModel):
    access_token: str
    refresh_token: str
    expires_in: int
    token_type: str
    scope: Optional[str] = None


@oauth_route.post("/token",  response_model=TokenResponse, responses={401: {"model": ErrorResponse}, 422: {"model": ErrorResponse}})
def get_token(session: Session = Depends(get_session), request: TokenRequest = Depends(TokenRequest.as_form)):
    if request.grant_type != 'password':
        raise CustomException(
            status_code=401,
            content={'error': 'Unauthorized', 'error_description': 'Wrong grant_type'}
        )

    client = get_client_by_client_id(session=session, client_id=request.client_id)
    if not client or client.client_secret != request.client_secret:
        raise CustomException(
            status_code=401,
            content={'error': 'Unauthorized', 'error_description': 'Client not found'}
        )

    user = get_user_by_username(session=session, username=request.username)
    if not user or user.password != request.password:
        raise CustomException(
            status_code=401,
            content={'error': 'Unauthorized', 'error_description': 'Wrong combination of username and password'}
        )

    inserted_token = generate_new_access_token_and_refresh_token(session=session, user_id=user.id,
                                                                 client_id=client.id)

    return TokenResponse(
        access_token=inserted_token.access_token,
        refresh_token=inserted_token.refresh_token,
        expires_in=300,
        token_type='Bearer'
    )


class ResourceResponse(BaseModel):
    access_token: str
    client_id: str
    user_id: str
    full_name: str
    npm: str
    expires: datetime
    refresh_token: str


@oauth_route.post("/resource", response_model=ResourceResponse, responses={401: {"model": ErrorResponse}, 422: {"model": ErrorResponse}})
def get_resource(authorization_token=Header(..., alias='Authorization', description='Bearer [token]'),
                 session: Session = Depends(get_session)):
    user, token = login(session=session, authorization_token=authorization_token)
    client = get_client_by_id(session=session, id=token.client_id)
    return ResourceResponse(
        access_token=token.access_token,
        client_id=client.client_id,
        user_id=user.username,
        full_name=user.full_name,
        npm=user.npm,
        expires=token.expired_at,
        refresh_token=token.refresh_token
    )
