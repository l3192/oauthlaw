sudo docker run --net=host --rm -v "$(pwd)/migrations/seeder:/liquibase/seeder" liquibase/liquibase \
    --url="jdbc:postgresql://localhost:5432/oauthlaw?currentSchema=oauthlaw" \
    --logLevel=debug \
    --changeLogFile=seeder/oauthlaw_seeder.sql \
    --username=postgres \
    --password=password update
