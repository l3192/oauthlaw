## OAuthLAW Service

--- 
### Created by:
Name: Ryo Axtonlie \
NPM: 1806205571 \
Major: Computer Science \
University: Universitas Indonesia \
Course: Layanan Aplikasi Web (Odd, 2021/2022)
---
### Disclaimer
This <b>OAuthLAW</b> services are made for Assignment 1 Layanan Aplikasi Web (Odd) 2021/2022

---
### Installation Guide (for Development)
#### Dependencies:
* Python (3.9.5) - [here](https://www.python.org/downloads/release/python-395/)
* Pipenv (Optional, you can use Python venv instead) - [here](https://pipenv.pypa.io/en/latest/)
* Docker - [here](https://www.docker.com/products/docker-desktop)
* PostgreSQL (Docker Image) - [here](https://hub.docker.com/_/postgres)
* Liquibase (Docker Image) - [here](https://hub.docker.com/r/liquibase/liquibase)

#### Installation Step:
1. Create Database \
   1. Download docker image postgres from [here](https://hub.docker.com/_/postgres)
      1. run container with command: 
         > docker run --name oauthlaw_postgres -e POSTGRES_PASSWORD=password -d -p 5432:5432 postgres
2. Access to PSQL
    - Access with CMD:
      > psql postgresql://postgres:mypassword@localhost
      
      or if you are using Docker (the step above can be used for Docker based PSQL as well)

      > docker exec -it oautchlaw_postgres psql -U postgres
    - Access via DataGrip:
      - Add Add New Data Source -> PostgreSQL
      - Name: oauthlaw_postgres@localhost
      - Host: localhost
      - Port: 5432
      - Authentication: User & Password
        - Username: <b>postgres</b>
        - Password: <b>password</b>
      - Database: postgres
      - URL: jdbc:postgresql://localhost:5432/postgres
3. Create Database with name `oauthlaw`
4. Create Schema with name `oauthlaw` on the Database `oauthlaw`
5. Run a migration script by using the command below (the script is used for every changes in database table you want to make):
   > ./migrate.sh
   
   (Please note this script only runnable for Docker Liquibase)
6. Run seeder script by using the command below
   > ./seeder.sh
   
   (Please note this script only runnable for Docker Liquibase)
7. Create a Virtual Environment
   1. If you choose to use Pipenv:
      1. Create or Activate the Virtual Environment on the main directory by using command below:
         > pipenv shell 
      2. Install the dependencies needed by using the command below:
         > pipenv install
   2. If you choose to use Python env:
      1. Create the Virtual Environment by using the command below (you can skip this part if you already created the Virtual Environment for this project):
         > python -m venv env
      2. Activate the Virtual Environment on the directory where you made it by using the command below:
         > source ./env/bin/activate
      3. Install the dependencies by using the command below:
         > pip install -r requirements.txt
   3. Run the app by running the command below on this project main directory: 
      > uvicorn oauthlaw_app.main:app --reload
8. Currently, to create new Client and User, you need to add it manually from database
