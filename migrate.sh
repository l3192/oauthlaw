sudo docker run --net=host --rm -v "$(pwd)/migrations/changelog:/liquibase/changelog" liquibase/liquibase \
    --url="jdbc:postgresql://localhost:5432/oauthlaw?currentSchema=oauthlaw" \
    --logLevel=debug \
    --changeLogFile=changelog/oauthlaw_migration.sql \
    --username=postgres \
    --password=password update
