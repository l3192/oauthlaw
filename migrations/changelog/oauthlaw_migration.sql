-- liquibase formatted sql

--changeset ryo.axtonlie:create-table-user
CREATE TABLE oauthlaw.user (
    id serial NOT NULL PRIMARY KEY,
    username varchar NOT NULL UNIQUE,
    password varchar NOT NULL,
    full_name varchar NOT NULL,
    npm varchar NOT NULL
);

--changeset ryo.axtonlie:create-table-client
CREATE TABLE oauthlaw.client (
    id serial NOT NULL PRIMARY KEY,
    client_id varchar NOT NULL UNIQUE,
    client_secret varchar NOT NULL
);

--changeset ryo.axtonlie:create-table-token
CREATE TABLE oauthlaw.token (
    id serial NOT NULL PRIMARY KEY,
    access_token varchar NOT NULL UNIQUE,
    refresh_token varchar NOT NULL UNIQUE,
    user_id integer NOT NULL
        REFERENCES oauthlaw.user,
    client_id integer NOT NULL
        REFERENCES oauthlaw.client,
    expired_at timestamp NOT NULL
);
