--liquibase formatted sql

--changeset ryo.axtonlie:create-client
INSERT INTO oauthlaw.client (client_id, client_secret) VALUES
('client1', 'client1'),
('client2', 'client2');

--changeset ryo.axtonlie:create-user
INSERT INTO oauthlaw.user (username, password, full_name, npm) VALUES
('user1', 'password1', 'User 1', '00000000001'),
('user2', 'password2', 'User 2', '00000000002');